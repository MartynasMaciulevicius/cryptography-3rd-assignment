/**
 * @author Martynas Maciulevičius.
 * @version 1.0 2015-09-18
 */
object Main {
  val keys = Array(47, 18, 96)
  val cipher = Array((5, 77), (6, 93), (11, 66), (29, 69), (5, 74), (27, 76), (6, 85), (11, 76), (3, 69), (29, 75), (7, 78), (14, 77), (17, 69), (13, 70), (9, 64), (7, 70), (2, 87), (11, 72), (25, 75), (14, 69), (9, 69), (11, 68), (4, 88), (3, 90), (23, 81), (8, 71), (21, 65), (6, 77), (3, 82), (3, 90), (25, 72), (0, 91), (11, 76), (22, 68), (6, 87), (9, 68), (24, 90), (30, 71), (19, 90), (5, 93), (9, 70), (3, 86), (6, 77), (25, 65), (7, 84), (4, 65), (15, 76), (3, 82), (3, 90), (17, 64), (29, 75), (5, 73), (19, 65), (18, 82), (3, 82), (23, 81), (3, 90), (17, 65), (11, 77), (7, 88))

  def main(args: Array[String]) {
        println(
          decode(keys, cipher, reducer(f))
        )

    val key2 = Array(175)
    val keysEnd = key2.iterator.toList
    val cipher2 = Array((74, 7), (68, 6), (76, 30), (90, 9), (90, 31), (64, 5), (74, 9), (70, 9), (89, 12), (65, 7), (75, 25), (70, 12), (70, 7), (76, 22), (83, 28), (76, 22), (78, 9), (80, 25), (90, 29), (89, 12), (76, 22), (69, 15), (65, 1), (76, 16), (70, 4), (92, 13), (77, 21), (80, 26), (91, 15), (92, 10), (80, 27), (76, 20), (90, 27), (75, 5), (80, 29), (70, 9), (83, 31), (77, 15), (70, 12), (69, 26), (74, 20), (70, 10), (64, 1), (86, 7), (70, 3))

    (1 to 50).map((i: Int) =>
      decode(keysEnd.::(i), cipher2, reducer(f2))
    )
      .filter(_.matches("^[A-Z ]+$"))
      .distinct
      .foreach(println(_))
  }

  def decode(keys: Seq[Int], cipher: Seq[(Int, Int)], r: ((Int, Int), Int) => (Int, Int)): String = {
    val out = cipher.map((data: (Int, Int)) =>
      keys.reverse.foldLeft[(Int, Int)](data)((data: (Int, Int), key: Int) =>
        r(data, key)
      )).map((tuple: (Int, Int)) => List(tuple._2, tuple._1))
      .foldLeft(List[Int]())((ints: List[Int], ints0: List[Int]) => ints ++ ints0)
      .map(_.toChar).toArray
    String.copyValueOf(out)
  }

  def reducer(iterFunc: (Int, Int) => Int)(data: (Int, Int), key: Int): (Int, Int) =
    (data._2, iterFunc(data._2, key) ^ data._1)

  def f(m: Int, k: Int): Int = (m ^ k) & ((k / 16) | m)

  def f2(m: Int, k: Int): Int = (m & k) | ((k % 16) ^ m)

}
